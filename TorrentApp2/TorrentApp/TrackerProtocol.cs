﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using MonoTorrent.BEncoding;
namespace TorrentApp
{
    class TrackerProtocol
    {

        private WebClient client;
        private HttpWebRequest request;

        private string status;

        public String Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public TrackerProtocol()
        {
            client = new WebClient();
            
        }

        public void setAttributes(String infoHash, String peerId, long port, double uploaded, double downloaded, double left, int compact, int noPeerId, Vars.EventState eventState)
        {
            client.QueryString.Add(Keys.infoHash, infoHash);
            client.QueryString.Add(Keys.peerId, peerId);
            client.QueryString.Add(Keys.port, "" + port);
            client.QueryString.Add(Keys.uploaded, "" + uploaded);
            client.QueryString.Add(Keys.downloaded, "" + downloaded);
            client.QueryString.Add(Keys.left, "" + left);
            client.QueryString.Add(Keys.compact, "" + compact);
            client.QueryString.Add(Keys.noPeerId, "" + noPeerId);
            client.QueryString.Add(Keys.eventState, Vars.getEventState(eventState));

    

        }

        public void setIp(string ipAddress)
        {
            client.QueryString.Add(Keys.ip, ipAddress);
        }

        public void setNumWant(long numWant)
        {
            client.QueryString.Add(Keys.numWant, "" + numWant);
        }

        public void setEvent(Vars.EventState eventState)
        {
            client.QueryString.Add(Keys.eventState, Vars.getEventState());
        }


        public string getResponse(string url)
        {


            string response = client.DownloadString(url);
            return response;
        }

        
        public int getTrackerInfo()
        {

            BEncodedDictionary dict = BEncodedDictionary.DecodeTorrent(Encoding.Default.GetBytes(Vars.trackerResponse));
            Vars.failureReason = Bencoding.getString(dict, Keys.failureReason);
            Vars.warningMessage = Bencoding.getString(dict, Keys.warningMessage);

            if (Vars.failureReason != null)
                return Keys.FAIL;

            
            BEncodedList list = dict[Keys.peers] as BEncodedList;


            Vars.peers = new List<Peer>();
            foreach(BEncodedDictionary peer in list)
            {
                
                IPAddress ip = IPAddress.Parse(Bencoding.getString(peer, Keys.ip));
                long port = Bencoding.getInt(peer, Keys.port);
                string peerId = Bencoding.getString(peer, Keys.peerId);
                Vars.peers.Add(new Peer(ip, port, peerId));
                
            }

            Vars.interval = Bencoding.getInt(dict, Keys.interval);
            Vars.minInterval = Bencoding.getInt(dict, Keys.minInterval);
            Vars.trackerId = Bencoding.getString(dict, Keys.trackerId);
            Vars.complete = Bencoding.getInt(dict, Keys.complete);
            Vars.incomplete = Bencoding.getInt(dict, Keys.incomplete);
            
         

            return Keys.SUCCESS;


        }

        public void testBencoding(int status)
        {
    
            if(status == Keys.FAIL)
            {Console.WriteLine(Keys.failureReason + " : \n\t" + Vars.failureReason);
             
            Console.WriteLine(Keys.warningMessage + " : \n\t" + Vars.warningMessage);
            }
            else{
            Console.WriteLine(Keys.interval + " : \n\t" + Vars.interval);
            Console.WriteLine(Keys.minInterval + " : \n\t" + Vars.minInterval);
            Console.WriteLine(Keys.trackerId + " : \n\t" + Vars.trackerId);
            Console.WriteLine(Keys.complete + " : \n\t" + Vars.complete);
            Console.WriteLine(Keys.incomplete + " : \n\t" + Vars.incomplete);
            /*Console.WriteLine(Keys.peerId + " :");
                foreach(Peer p in Vars.peers)
                    Console.WriteLine("\n\t" + p.ToString());
             */

            Console.WriteLine(Keys.peerId + " length : \n\t" + Vars.peers.Count);
            }
   

         }
        }


    }


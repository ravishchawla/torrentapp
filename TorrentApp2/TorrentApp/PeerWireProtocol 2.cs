﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Net.NetworkInformation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
namespace TorrentApp
{
    class PeerWireProtocol
    {
        
        
        Peer peer;
        TcpClient client;
        Socket socket;
        int completed = 0;
        byte[] sendBuffer;
        bool nextOneIsHaves = false;
        Boolean hasRequestedBefore = false;
        Boolean hasSentInterstedBefore = false;
        Boolean hasGottenHaveMesssages = false;
        long bufferSize = 1024;
        int totalBytesRead = 0;


        public PeerWireProtocol()
        {
         //   getPeer();
            
           for(int i =0; i < Vars.peers.Count; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(setUpTCPConnection));
                t.Start((Int32)i);
            }

            System.Threading.Thread.Sleep(1000);

            int next = new Random().Next(Vars.connections.Count);
            socket = Vars.connections.ElementAt(next);
            Console.WriteLine("!Connected to " + Vars.connectedPeers.ElementAt(next).Ip.ToString());
            
      //    socket =   setUpSingleTcpConnection("81.166.137.145", 18296); //sends cancel message
      //      socket = setUpSingleTcpConnection("96.126.107.188", 51413); //doesn't send pieces
     //       socket = setUpSingleTcpConnection("195.202.205.112", 37329);
    //        socket = setUpSingleTcpConnection("208.54.223.15", 57486);

            send(Message.getHandshake());
            Thread thread = new Thread(new ThreadStart(listen));
            thread.Start();


         //   Thread sendThread = n , 0ew Thread(new ThreadStart(sendStuff));
           // sendThread.Start();

            var timer = new System.Threading.Timer((e) =>
            {
                sendKeepAlive();

            }, null, 0, (long)TimeSpan.FromMinutes(2).TotalMilliseconds);

            var listener = new System.Threading.Timer((e) =>
            {
                listen();


            }, null, 0, (long)TimeSpan.FromMilliseconds(50).TotalMilliseconds);
            
            
         //   receive();
         }
                           

        public Peer getPeer()
        {

          return Vars.peer = Vars.connectedPeers.ElementAt(new Random().Next(Vars.connectedPeers.Count - 1));

        }


        public Socket setUpSingleTcpConnection(string ip, long port)
        {
            try
            {
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(ip, (Int32)port);

                return socket;
            }
            catch
            {
                Console.WriteLine("couldn't connect to host " + ip);
                return null;
            }
        }
        public void setUpTCPConnection(Object pos)
        {

            Peer peer = Vars.peers.ElementAt((Int32)pos);  

           // Console.WriteLine("connecting to ..." + peer.Ip + " at " + (Int32)peer.Port);
            try
            {


                IPEndPoint point = new IPEndPoint(peer.Ip, (Int32)peer.Port);
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(point);
                //client = new TcpClient();
                
                //client.Connect(peer.Ip, (Int32)peer.Port);
                completed++;
                Console.WriteLine((Int32)pos + ": Connected " + peer.ToString());
                Vars.connectedPeers.Add(peer);
                Vars.connections.Add(socket);
            
            }

            catch(Exception exe)
            {
                client = null;
                completed++;
          //      Console.WriteLine("Timeout");
            //    Console.WriteLine(exe.Message);
            }


        }

        
      

        public void listen()
        {
            byte[] buffer = new byte[1024];
            Console.WriteLine("listening..");

                if(socket.Poll(-1, SelectMode.SelectRead) && socket.Available > 0)
                {
                    

                    Console.WriteLine("\nData avalaible for Reading");
                    buffer = new byte[bufferSize];
                    socket.Receive(buffer);
                    Console.WriteLine("Data ->\t");
                    for (int i = 0; i < 256; i++ )
                            Console.Write(buffer[i].ToString("X") + " ");

                    Console.WriteLine();

                  //  sendIntersted(new byte[5]);

                    bufferSize = 1024;
                    printMessage(buffer);
                   // Console.Read();

                }
            
        }

        public void printMessage(byte[] buffer)
        {
            int ret = 1;
            int pos = 4;

            while (ret == 1)
            {

                Console.WriteLine("stuck: " + buffer[pos]);
                if (buffer[0] == 19)
                {
                    ret = HandleHandShake(ref buffer);
                    nextOneIsHaves = true;
                    continue;
                }

                else if (nextOneIsHaves)
                {
                    Console.WriteLine("calling bitfiled");
                    ret = handleBitField(buffer);
                    nextOneIsHaves = false;
                    continue;
                }

            //    else if ((buffer[1] == 5 && buffer[2] == 4) || (buffer[0] == 5 && buffer[1] == 4) || (buffer[8] == 5 && buffer[9] == 4))
                else if (hasSentInterstedBefore && !hasGottenHaveMesssages)
                {
                    ret = handleBitField(buffer);
                    hasGottenHaveMesssages = true;
                    continue;
                }

                else  if (buffer[pos - 1] == 0 && buffer[pos - 2] == 0 && buffer[pos - 3] == 0 && buffer[pos] == 0)
                    ret = 0;

                else if (buffer[pos - 1] == 1 && buffer[pos - 2] == 0 && buffer[pos - 3] == 0 && buffer[pos] == 1 && !hasRequestedBefore)
                    ret = handleUnchoke(buffer);

                else if (buffer[pos - 1] == 0 && buffer[pos - 2] == 0 && buffer[pos - 3] == 0 && buffer[pos] == 2)
                    Console.WriteLine("INTERSTED");

                else if (buffer[pos - 1] == 0 && buffer[pos - 2] == 0 && buffer[pos - 3] == 0 && buffer[pos] == 3)
                    Console.WriteLine("UNINTERSTED");

              //  else if (buffer[pos - 1] == 5 && buffer[pos - 2] == 0 && buffer[pos - 3] == 0 && buffer[pos] == 4 && !hasRequestedBefore)
               //     Console.WriteLine("HAVE");

                else if (buffer[pos] == 5 && !hasRequestedBefore)
                    ret = handleBitField(buffer);

                else if (buffer[pos - 1] == 0 && buffer[pos - 2] == 0 && buffer[pos - 3] == 0 && buffer[pos] == 6)
                {
                    Console.WriteLine("REQUEST");
                }
                else if (hasRequestedBefore)
                {
                    ret = readPieces(buffer);
                    Console.Read();
                    
                }

                else if (buffer[pos - 1] == 0 && buffer[pos - 2] == 0 && buffer[pos - 3] == 0 && buffer[pos] == 8)
                    Console.WriteLine("CANCEL");

                else if (totalBytesRead <= Vars.pieceLength)
                    ret = concatinatePiece(ref buffer);
                else
                {
                    Console.Read();
                    Console.WriteLine("STOP");
                }


              /*  else
                    switch (buffer[pos])
                    {
                        case (0):
                            ret = 0;
                            break;
                        case (1):
                            ret = handleUnchoke(buffer);
                            break;
                        case (2):
                            if (buffer[pos - 1] == 0 && buffer[pos - 2] == 0)
                                Console.Error.WriteLine("INTERESTED");
                            else
                                ret = 0;
                            break;
                        case (3):
                            if (buffer[pos - 1] == 0 && buffer[pos - 2] == 0)
                                Console.Error.WriteLine("UNINTERSTED");
                            else
                                ret = 0;
                            break;
                        case (4):
                            Console.Error.WriteLine("HAVE");
                            break;
                        case (5):
                            ret = handleBitField(buffer);
                            break;
                        case (6):
                            Console.Error.WriteLine("REQUEST");
                            break;
                        case (7):
                            if (buffer[pos - 2] == 0 && buffer[pos - 1] == 0 && buffer[pos] == 0 && buffer[pos] == 0)
                                ret = readPieces(buffer);
                            break;
                        case (8):
                            Console.Error.WriteLine("CANCEL");
                            break;
                        default:
                            ret = concatinate(buffer);
                            break;
                

                    }
               * 
               * */
            }

            

            if(ret == 2)
            {
                sendIntersted(buffer);
                hasSentInterstedBefore = true;
            }
        }



        public int HandleHandShake(ref byte[] buffer)
        {
            Console.WriteLine("Handle handshake");
            if (buffer[0] != 19)
                return -1;
            else
            {
                Byte[] protocol = new List<byte>(buffer).GetRange(1, Keys.protocolLength).ToArray();

                if (Utility.byteToStringArray(protocol).Equals(Keys.protocol))
                {
                    Byte[] reserved = new List<byte>(buffer).GetRange(protocol.Length + 1, 8).ToArray();

                    Byte[] hash = new List<byte>(buffer).GetRange(protocol.Length + reserved.Length + 1, 20).ToArray();

                    Byte[] id = new List<byte>(buffer).GetRange(protocol.Length + reserved.Length + hash.Length + 1, 20).ToArray();


                    int total = protocol.Length + reserved.Length + hash.Length + id.Length + 1;
                    byte[] restOfTheMessage = new List<byte>(buffer).GetRange(total, buffer.Length - total).ToArray();

                    Console.WriteLine("rest of the message: \n\t" + restOfTheMessage[4].ToString("X"));

                    buffer = restOfTheMessage;
                    return 1;
                    

                }


            }

            return 0;
        }

        public int handleBitField(byte[] buffer)
        {
            
                
            for (int i = 0; i < buffer.Length - 4; i++)
            {
                if (buffer[i] == 0 && buffer[i + 1] == 0 && buffer[i + 2] == 0 && buffer[i + 3] == 5 && buffer[i + 4] == 4)
                {
                    //Vars.actualPieces.Add(new Piece(new List<byte>(buffer).GetRange(i + 5, 4).ToArray()));

                    //int index = Utility.convertToDecimalFromDecimalBytes(new List<byte>(buffer).GetRange(i + 5, 4).ToArray());

                    int index = buffer[i + 8];
                    Vars.actualPieces[index] = new Piece(new List<byte>(buffer).GetRange(i+5,4).ToArray());
                }
            }   


            Console.WriteLine("Have Messages found: \n\t");
            foreach(Piece piece in Vars.actualPieces)
            {
                if (piece == null)
                    continue;

                byte[] barray = piece.pieceIndex;
                foreach (byte b in barray)  
                    Console.Write(b.ToString("X") + " ");

                Console.WriteLine();
            }

                return 2;
        }

        public int handleUnchoke(byte[] buffer)
        {
            Console.WriteLine("in handle unchocked");
            hasRequestedBefore = true;

            for(int i = 0; i < Vars.actualPieces.Count(); i++)
            {
                if (Vars.actualPieces.ElementAt(i) == null)
                    continue;

                Thread th = new Thread(new ParameterizedThreadStart(getPieces));
                th.Start(i);
            }
       

            return 0;
        }

        public void getPieces(object pos)
        {

            Message first = new Message(6);
            byte[] request = Vars.actualPieces.ElementAt((Int32)pos).pieceIndex;
            Array.Reverse(request);
            Console.WriteLine("requesting: \t" + Utility.byteToString(request));
            byte[] blockOffset = BitConverter.GetBytes(0);
            byte[] length = BitConverter.GetBytes(Keys.maxRequestLength);
            first.addPayload(request);
            first.addPayload(blockOffset);
            first.addPayload(length);

            socket.Send(first.createMessage());


        }

        public int readPieces(byte[] buffer)
        {
            Console.WriteLine("reading pieces");
            

            byte[] length = new List<byte>(buffer).GetRange(0, 4).ToArray(); //0 1 2 3
            Console.WriteLine(Utility.byteToString(length));
            byte id = buffer[4];                                             //4
            Console.WriteLine("ID: \t\t" + id);
            byte[] payload = new List<byte>(buffer).GetRange(5, 4).ToArray(); //5 6 7 8
            Console.WriteLine(Utility.byteToString(payload));
            byte[] blockOffset = new List<byte>(buffer).GetRange(9, 4).ToArray(); //9 10 11 12
            Console.WriteLine(Utility.byteToString(blockOffset));
            
            byte[] restOfTheData = new List<byte>(buffer).GetRange(13, buffer.Count() - 13).ToArray(); //13 ..
            Vars.data.AddRange(restOfTheData);

            totalBytesRead += (buffer.Count() - 13);

            Vars.pieceLength = Utility.convertToDecimalFromHex(length);
            Console.WriteLine("Vars.pieceLength: " + Vars.pieceLength);

            Vars.currentPiece = Vars.getPiece(payload);
           
            return 0;

        }

        public int concatinatePiece(ref byte[] buffer)
        {

            
            //totalBytesRead += buffer.Count();
            Console.WriteLine("Concatinating with id = " + buffer[4]);
            foreach(byte b in buffer)
            {
                totalBytesRead++;
                if(totalBytesRead > Vars.pieceLength)
                {
                    byte[] newbuffer = new List<byte>(buffer).GetRange(totalBytesRead, buffer.Count() - totalBytesRead).ToArray();

                    buffer = newbuffer;
                    return 1;
                }
            }

            
            Console.WriteLine("Total bytes read: " + totalBytesRead);
            Vars.currentPiece.addData(buffer);

     

            return 0;
        }

        public void sendIntersted(byte[] buffer)
        {
            Message message = new Message(2);

            socket.Send(message.createMessage());

            Console.WriteLine("sent intersted message");
        }

        public void sendKeepAlive()
        {
            Console.WriteLine("sending keep alive");

            sendBuffer = Message.getKeepAlive();
         //   isAvalaible = true;
            
                
            
        }
        

        

        public void send(byte[] bytes)
        {

          
         //   socket = Vars.connections.ElementAt(0);
            Console.WriteLine("Connection: " + socket.Connected);

            try
            {
                socket.Send(bytes);
                
         //       Console.WriteLine("Sent to " + Vars.connectedPeers.ElementAt(0).Ip.ToString() + ", " + Vars.connectedPeers.ElementAt(0).Port);


            }
            catch(Exception exe)
            {
                Console.WriteLine(exe.Message);


            }

        }

        public void receive()
        {
            byte[] buffer = new byte[1024];
            
            if (socket.Connected)
            {
                socket.Receive(buffer);
                

                foreach (byte b in buffer)
                    Console.Write(b);

            }
            else
                Console.WriteLine("not connected");

        }

        public void testConnection()
        {
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpInfoList = properties.GetActiveTcpConnections();

            foreach(TcpConnectionInformation info in tcpInfoList)
            {
                Console.WriteLine("<" + info.RemoteEndPoint.Address.ToString() + ", " + info.State.ToString() + ">");
            }

        }

        


    }

}

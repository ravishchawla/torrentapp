﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BencodeLibrary;
using System.IO;
using System.Text.RegularExpressions;

namespace TorrentApp
{
    class Program
    {
        static void Main(string[] args)
        {
            new Keys();
            new Vars();
            new Utility();
            ReadFile file = new ReadFile();
            TrackerProtocol tracker = new TrackerProtocol();

            
            



            Console.WriteLine("\n============================================================");
            Console.WriteLine("                       Parsing Torrent ");
            Console.WriteLine("=============================================================\n");
            file.readFile("rhcpiamwithyou.torrent");
            file.testBencoding(); 
            
            file.getInfoHash();
            tracker.setAttributes(Vars.infoHashUrl, Vars.peerID, Vars.port, Vars.uploaded, Vars.downloaded, Vars.left, Vars.compact, Vars.noPeerId, Vars.currentEventState);
           String output =  tracker.getResponse(Vars.announceUrl);

           Console.WriteLine("\n============================================================");
           Console.WriteLine("                      Connecting to Tracker ");
           Console.WriteLine("=============================================================\n");

           Vars.trackerResponse = Encoding.UTF8.GetString(Encoding.Default.GetBytes(output)); ;
           Console.WriteLine("Tracker Response: \n" + Vars.trackerResponse);

           Console.WriteLine("\n============================================================");
           Console.WriteLine("                     Parsing Tracker Response ");
           Console.WriteLine("=============================================================\n");

            int status = tracker.getTrackerInfo();
            tracker.testBencoding(status);

            Console.WriteLine("\n============================================================");
            Console.WriteLine("               Initialize TCP Connections with Peers");
            Console.WriteLine("=============================================================\n");

            PeerWireProtocol peerwire = new PeerWireProtocol();


        
           // Console.WriteLine("exitting");
            Console.Read();
            Utility.saveFina1DataToDiskAndChecksum();
        }

    }
}

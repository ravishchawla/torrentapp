﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MonoTorrent.BEncoding;
using BencodeLibrary;
using System.IO;
using System.Threading;
namespace TorrentApp
{
 


    class Bencoding
    {

        public static string getString(BDict file, String key)
        {


            try
            {
                return (file[key] as BString).Value;


            }

            catch (Exception exe)
            {
                return null;
            }


        }

        public static long getInt(BDict file, String key)
        {


            try
            {
                return (file[key] as BInt).Value;
            }

            catch (Exception exe)
            {
                return -1;
            }



        }

        public static BList getList(BDict file, String key)
        {
            try
            {
                return (file[key] as BList);

            }

            catch (Exception exe)
            {
                return null;
            }
        }

        public static BDict getDict(BDict file, String key)
        {


            try
            {
                return (file[key] as BDict);
            }

            catch (Exception exe)
            {
                return null;
            }


        }


        public static string getString(BEncodedDictionary file, String key)
        {


            try
            {
                return (file[key] as BEncodedString).Text;


            }

            catch (Exception exe)
            {
                return null;
            }


        }

        public static long getInt(BEncodedDictionary file, String key)
        {


            try
            {
                return (file[key] as BEncodedNumber).Number;
            }

            catch (Exception exe)
            {
                return -1;
            }



        }

        public static BEncodedList getList(BEncodedDictionary file, String key)
        {
            try
            {
                return (file[key] as BEncodedList);

            }

            catch (Exception exe)
            {
                return null;
            }
        }

        public static BEncodedDictionary getDict(BEncodedDictionary file, String key)
        {


            try
            {
                return (file[key] as BEncodedDictionary);
            }

            catch (Exception exe)
            {
                return null;
            }


        }

        public List<string> getAccounceString(string torrent)
        {


            List<String> links = new List<string>();
            string link = "";
            Regex regex = new Regex(@"http:.*?(?=\d+:)");

            foreach (Match match in regex.Matches(torrent))
            {
                if (match.Success)
                    links.Add(link = match.Value);
            }


            link = link.Substring(0, link.Length - 1);
            links.RemoveAt(links.Count - 1);
            links.Add(link);

            return links;



        }

        public static string getIntegerValue(string key, string torrent)
        {
            Regex regex = new Regex(":" + key + "i(\\d)+e");
            Match match = regex.Match(torrent);

            if (!match.Success)
                return null;

            string output = match.Value;

            regex = new Regex(@"\d+");
            match = regex.Match(output);

            return match.Value;
        }

        public static string getSpecialIntegerValue(string key, string torrent)
        {
            Regex regex = new Regex(":" + key + "(\\d)+");
            Match match = regex.Match(torrent);

            if (!match.Success)
                return null;

            string output = match.Value;

            regex = new Regex(@"\d+");
            match = regex.Match(output);

            return match.Value;

        }

        public static string getStringValue(string key, string torrent)
        {
            //(?<=:name\d+:)^?.*
            Regex regex = new Regex("(?<=:" + key + "\\d+:)^?.*?(?=\\d+:)");
            Match match = regex.Match(torrent);

            if (!match.Success)
                return null;

            string output = match.Value;

            return output;


        }

        public static string getSpecialStringValue(string key, string torrent)
        {
            Regex regex = new Regex("(?<=:" + key + "\\d+:)^?.*");
            Match match = regex.Match(torrent);

            if (!match.Success)
                return null;

            string output = match.Value;
            return output;
        }



    }

    class Message
    {


        public UInt32 length;
        public byte ID;
        public List<byte> payload;

        public Message(byte id)
        {
            ID = id;
            length = 1;
            payload = new List<byte>();
        }

        public void addPayload(byte[] d)
        {
            Array.Reverse(d);
            payload.AddRange(d);
            
            length += (uint)d.Length;
        }

        public byte[] createMessage()
        {
            UInt32 newLength = (UInt32)(1 + payload.Count);
            byte[] messageInBytes = new Byte[length + 4];
            byte[] lengthInBytes = BitConverter.GetBytes(length);
            Array.Reverse(lengthInBytes);             
            lengthInBytes.CopyTo(messageInBytes, 0);
            messageInBytes[4] = ID;
            payload.CopyTo(messageInBytes, 5);

            return messageInBytes;
        }

        public static byte[] getHandshake()
        {
            //       byte[] sendBuf = (new[] { (byte)Keys.protocolLength }).Concat(GetBytes(Keys.protocol)).Concat(new Byte[] {0,0,0,0,0,0,0,0}).Concat(GetBytes(Vars.infoHash)).Concat(GetBytes(Vars.peerID)).ToArray();
            byte[] sendBuf = (new[] { (byte)Keys.protocolLength }).Concat(Utility.GetBytes(Keys.protocol)).Concat(new Byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }).Concat(Utility.StringToByteArray(Vars.infoHash, 2)).Concat(Utility.StringToByteArray(Vars.peerID, 1)).ToArray();
            //byte[] first = (new[] { (byte)Keys.protocolLength }).Concat(GetBytes(Keys.protocol)).Concat(new Byte[]{0,0,0,0,0,0,0,0}).ToArray();
            //byte[] second = StringToByteArray(Vars.infoHash).Concat(StringToByteArray(Vars.peerID)).ToArray();



            return sendBuf;


        }

        public static byte[] getKeepAlive()
        {
            return BitConverter.GetBytes((Int32)0);

            
        }

        public static byte[] getChoke()
        {
            Message message = new Message(0);

            return message.createMessage();
        }

        public static byte[] getUnChoke()
        {
            Message message = new Message(1);

            return message.createMessage();
        }

        public static byte[] sendIntersted()
        {
            Message message = new Message(2);
            return message.createMessage();
        }

        public static byte[] sendUnIntersted()
        {
            Message message = new Message(3);
            return message.createMessage();
        }

        public static byte[] sendHave(UInt32 index)
        {
            Message message = new Message(4);
            message.addPayload(BitConverter.GetBytes(index));
            return message.createMessage();
            
        }

   



    }


    class Utility
    {

        public Utility()
        {
            
        //    Thread thread = new Thread(new ThreadStart(checkConnectionStatus));
          //  thread.Start();


            
        }
       
      public  static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);


            byte[] newbytes = new byte[bytes.Length / 2];
            for (int i = 0, j = 0; i < bytes.Length; i += 2, j++)
            {
                newbytes[j] = bytes[i];
            }

            return newbytes;
        }

       
        public static string byteToStringArray(byte[] arr)
      {
          string ans = "";
            foreach(byte b in arr)
            {
                ans += (char)b;
            }

            return ans;
      }

        public static string byteToString(byte[] arr)
        {
            string ans = "";
            foreach (byte b in arr)
            {
                ans += b;
            }

            return ans;
        }

        public static byte[] StringToByteArray(string hex, int split)
        {

            
            //Console.WriteLine("Converting\n\t" + hex);

            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % split == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, split), 16))
                             .ToArray();
        }

        public static void checkConnectionStatus()
        {
            Vars.statusTwo = DateTime.Now.Millisecond;
            if((Vars.statusTwo - Vars.statusOne > 6000))
            {
                
                Environment.Exit(0);
            }
        }

        public static void printPieces()
        {
            

            foreach (Piece piece in Vars.actualPieces)
            {
            Console.WriteLine(piece.pieceOffset + "\t" + Vars.actualPieces.IndexOf(piece));
                
                    

                Console.WriteLine();
            }
        }

        public static void updateFields()
        {
            

        }

        public static void saveFinalDataToDiskAndChecksum()
        {
            try
            {

                string checksum = Utility.calculateSHash(Vars.currentPiece.data);

                if (checksum.Equals(Vars.checksum))
                {

                    using (StreamWriter writer = new StreamWriter("out.txt"))
                    {
                        foreach (char b in Keys.aggregateData)
                        {
                            writer.Write(b);
                            //Console.Write(b);
                        }

                        writer.Close();
                    }
                }
                else
                {
                    Console.Out.WriteLine("Checksum did not complete");
                }


            }

            catch(Exception exe)
            {

            }

        }

        public static int convertToDecimalFromHex(byte[] hex)
        {
            byte[] hexbytes = new byte[hex.Count()];
            for (int i = 0; i < hex.Count(); i++ )
            {
                string hexstr = hex[i].ToString("X");
                Int32 int32;
                try
                {
                    int32 = Int32.Parse(hexstr);
                }
                catch (Exception e)
                {
                    int32 = Convert.ToInt32(hexstr, 16);

                }
                byte byte32 = (byte)int32;

                hexbytes[i] = byte32;
            }


            

            byte[] newbytes = splitBytes(hexbytes);

           // byte[] newbytes = hex;
            foreach(byte b in newbytes)
                //Console.WriteLine(b + " +'t");

            Array.Reverse(newbytes);
            int multiplier = 1;
            int dec = 0;

            foreach (byte b in newbytes)
            {
                dec += b * multiplier;
                multiplier *= 16;
                //Console.WriteLine(dec);
            }

            return dec;

            //[40] [9] //[4] [0] [0] [9]
            //[9] [40]
            //dec = 9*1 = 9
            //dec = 9 + 0

        }


        public static int convertToDecimalFromHexBytes(byte[] bytes)
        {
            byte[] newbytes = splitBytes(bytes);
            string str = "";
            foreach (byte b in newbytes)
                str += b;

            //99
            return Convert.ToInt32(str, 16);
            //153

        }

        public static int convertToDecimalFromDecimalBytes(byte[] bytes)
        {
            byte[] newbytes = splitBytes(bytes);
            string str = "";
            foreach (byte b in newbytes)
                str += b;

            return Convert.ToInt32(str);
        }


        public static byte[] splitBytes(byte[] bytes)
        {
            List<byte> newbytes = new List<byte>();
            for (int i = 0; i < bytes.Count(); i++ )
            {


                if ((i == 0 && bytes[i] == 0) || ( bytes[i] == 0 && bytes[i - 1] == 0))
                    continue;
                newbytes.Add((byte)(bytes[i] / 10));
                newbytes.Add((byte)(bytes[i] % 10));


            }


            return newbytes.ToArray();
        }



        public static void saveFina1DataToDiskAndChecksum()
        {
            if (Vars.piecesRead > Vars.totalPieces)
                System.IO.File.Copy("debian.torrent", "out.txt");
            else
                Console.Out.WriteLine("Checksum did not complete");
        }




        
        public static string calculateSHash(Object data)
        {
            try
            {
                BDict bData = (BDict)data;

                return BencodeLibrary.BencodingUtils.CalculateTorrentInfoHash(bData).ToString();


            }
            catch(Exception e)
            {
                return null;
            }
        }


    }


}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using OSS.NBEncode.Transforms;
using OSS.NBEncode.Entities;



namespace TorrentApp
{
    class ReadFile
    {
        public static void main()
        {

            ReadFile readfile = new ReadFile();
            readfile.readFile("ubuntu.torrent");
            
        }

        public void readFile(String fileName)
        {
            StreamReader reader = new StreamReader(fileName);





            string readText;
            string torrent = "";
            while ((readText = reader.ReadLine()) != null)
            {
                torrent = torrent + readText;

            }
            reader.Close();


            List<string> links = getAccounceString(torrent);
            foreach (string link in links)
                Console.WriteLine(link);


            Console.WriteLine(getIntegerValue("creation date", torrent));
            Console.WriteLine(getIntegerValue("length", torrent));
            Console.WriteLine(getStringValue("comment", torrent));
            Console.WriteLine(getIntegerValue("piece length", torrent));
            Console.WriteLine(getSpecialIntegerValue("pieces", torrent));
            Console.WriteLine(getStringValue("name", torrent));

            Console.Read();

        }


        public void test(String torrent)
        {

            

        }


        public void testBencoding(String torrent)
        {

            var inputStream = new MemoryStream(new byte[] { 52, 58, 49, 50, 51, 52 }, false);       // "4:1234"
            var expectedOutput = new byte[] { 49, 50, 51, 52 };



            var inFileStream = new FileStream(torrent, FileMode.Open);

            var bot = new BObjectTransform();
            IBObject decodeString = bot.DecodeNext(inFileStream);
            BDictionary cast = (BDictionary)decodeString;

            

            BByteString bstring = (BByteString)cast.Value[new BByteString("announce")];

            Console.WriteLine(bstring.ConvertToText(Encoding.UTF8));

            Console.WriteLine("len: " + inFileStream.Length + "\npos: " + inFileStream.Position);
            ListTransform listTranform = (ListTransform)bot.DecodeNext(inFileStream);

            BList list = (BList)listTranform.Decode(inFileStream);

            
            //bstring = (BByteString)list.Value[0];

            Console.WriteLine(list.Value.Length);
        }

        public List<string> getAccounceString(string torrent)
        {


            List<String> links = new List<string>();
            string link = "";
            Regex regex = new Regex(@"http:.*?(?=\d+:)");

            foreach (Match match in regex.Matches(torrent))
            {
                if (match.Success)
                    links.Add(link = match.Value);
            }


            link = link.Substring(0, link.Length - 1);
            links.RemoveAt(links.Count - 1);
            links.Add(link);

            return links;



        }

        public string getIntegerValue(string key, string torrent)
        {
            Regex regex = new Regex(":" + key + "i(\\d)+e");
            Match match = regex.Match(torrent);

            string output = match.Value;

            regex = new Regex(@"\d+");
            match = regex.Match(output);

            return match.Value;
        }

        public string getSpecialIntegerValue(string key, string torrent)
        {
            Regex regex = new Regex(":" + key + "(\\d)+");
            Match match = regex.Match(torrent);

            string output = match.Value;

            regex = new Regex(@"\d+");
            match = regex.Match(output);

            return match.Value;

        }

        public string getStringValue(string key, string torrent)
        {
            //(?<=:name\d+:)^?.*
            Regex regex = new Regex("(?<=:" + key + "\\d+:)^?.*?(?=\\d+:)");
            Match match = regex.Match(torrent);
            string output = match.Value;

            return output;


        }


    }
}

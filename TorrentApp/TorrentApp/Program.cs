﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BencodeLibrary;
using System.Text.RegularExpressions;

namespace TorrentApp
{
    class Program
    {
        static void Main(string[] args)
        {
            new Vars();
            new Utility();
            ReadFile file = new ReadFile();
            TrackerProtocol tracker = new TrackerProtocol();

            file.readFile("dsl.torrent");
            file.testBencoding(); 
            
            file.getInfoHash();
            tracker.setAttributes(Vars.infoHashUrl, Vars.peerID, Vars.port, Vars.uploaded, Vars.downloaded, Vars.left, Vars.compact, Vars.noPeerId, Vars.currentEventState);
           String output =  tracker.getResponse(Vars.announceUrl);

           Vars.trackerResponse = Encoding.UTF8.GetString(Encoding.Default.GetBytes(output)); ;

 
            Console.WriteLine("OUtput: \n\t" + Vars.trackerResponse);

            int status = tracker.getTrackerInfo();
            tracker.testBencoding(status);

            PeerWireProtocol peerwire = new PeerWireProtocol();

            Console.Read();
        }

    }
}

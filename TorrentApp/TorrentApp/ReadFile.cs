﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using BencodeLibrary;
using System.Net;


namespace TorrentApp
{
    class ReadFile
    {
        public void readFile(String torrent)
        {

            BDict torrentFile = BencodingUtils.DecodeFile(torrent) as BDict;

            Vars.announceUrl = Bencoding.getString(torrentFile, Keys.announceUrlString);



            BList list = Bencoding.getList(torrentFile, Keys.announceListString);
            if (list != null)
            {
                Vars.announceList = list.OfType<BList>().Select(element => (element.First() as BString).Value).ToList();
            }

            Vars.comment = Bencoding.getString(torrentFile, Keys.commentString);

            Vars.createdBy = Bencoding.getString(torrentFile, Keys.createdByString);
            Vars.creationDate = Bencoding.getString(torrentFile, Keys.creationDateString);

            BDict info = Bencoding.getDict(torrentFile, Keys.infoString);
            Vars.info = info;

            Vars.length = Bencoding.getInt(info, Keys.lengthString);
            Vars.checksum = Bencoding.getString(info, Keys.checksumString);
            Vars.name = Bencoding.getString(info, Keys.nameString);
            Vars.pieceLength = Bencoding.getInt(info, Keys.pieceLengthString);
            Vars.pieces = Bencoding.getString(info, Keys.piecesString);

 



        }




        public void getInfoHash()
        {

            Byte[] hash = BencodingUtils.CalculateTorrentInfoHash(Vars.info);

            Vars.infoHashUrl = (byteArrayToStringUrl(hash));
            Vars.infoHash = (byteArrayToString(hash));
            Console.WriteLine("infohash: \n\t" + Vars.infoHash);


        }

        public static string byteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }


        public static string byteArrayToStringUrl(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("%{0:x2}", b);
            return hex.ToString();
        }

        public void testBencoding()
        {
            Console.WriteLine(Keys.announceUrlString + " : \n\t" + Vars.announceUrl);

            Console.WriteLine(Keys.announceListString + " : ");
            if (Vars.announceList != null)
                foreach (String url in Vars.announceList)
                    Console.WriteLine("\t" + url);

            Console.WriteLine(Keys.commentString + " : \n\t" + Vars.comment);
            Console.WriteLine(Keys.createdByString + " : \n\t" + Vars.createdBy);
            Console.WriteLine(Keys.creationDateString + " : \n\t" + Vars.creationDate);
            Console.WriteLine(Keys.lengthString + " : \n\t" + Vars.length);
            Console.WriteLine(Keys.checksumString + " : \n\t" + Vars.checksum);
            Console.WriteLine(Keys.nameString + " : \n\t" + Vars.name);
            Console.WriteLine(Keys.pieceLengthString + " : \n\t" + Vars.pieceLength);
            //    Console.WriteLine(Keys.piecesString + " : " + Vars.pieces);

            byte[] piecesBytes = Utility.GetBytes(Vars.pieces);

        //    int pos = 0;

         /*   foreach (byte b in piecesBytes)
            {
                Console.Write(b.ToString("X") + " ");

                if (pos++ > 20)
                {
                    Console.WriteLine();
                    pos = 0;
                }
            }
            Console.ReadLine();



            */

        }
    }
}

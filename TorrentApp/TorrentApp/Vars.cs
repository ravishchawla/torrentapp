﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoTorrent.BEncoding;
using BencodeLibrary;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.IO;
using TorrentApp;

namespace TorrentApp
{
    class Vars
    {
        
        public static BDict  info;
        public static string announceUrl;
        public static List<String> announceList;
        public static string comment;
        public static string createdBy;
        public static string creationDate;

        
        public static long length;
        public static string checksum;
        public static string name;
        public static long pieceLength;
        public static string pieces;

        public static string infoHashUrl;
        public static string infoHash;
        public static string peerID;
        public static long port;
        public static double uploaded;
        public static double downloaded;
        public static double left;
        public static int compact;
        public static int noPeerId;
        public static string attributes;
        public static string trackerResponse;

        public static string failureReason;
        public static string warningMessage;
        public static long interval;
        public static long minInterval;
        public static string trackerId;
        public static long complete;
        public static long incomplete;
        public static List<Peer> peers;
        public static List<Peer> connectedPeers;
        public static List<Socket> connections;
        public static List<Piece> actualPieces;
        public static List<Piece> havePieces;
        public static List<byte> data;
        public static long blockOffset = 0;

        public static Peer peer;
        public static Piece currentPiece;

        public enum EventState { started, stopped, completed };
        public static EventState currentEventState;

     //   public static StreamWriter writer = new StreamWriter("out.txt");
        public static DateTime statusOne, statusTwo;

        public Vars()
        {

            generateRandomPeerId();
            port = 6689;
            currentEventState = EventState.started;
            compact = 0;
            noPeerId = 1;
            uploaded = 0.0;
            downloaded = 0.0;
            left = 0.0;
            peers = new List<Peer>();
            connectedPeers = new List<Peer>();
            connections = new List<Socket>();
            actualPieces = new List<Piece>();
            //Console.SetOut(writer);
            data = new List<byte>();
            statusOne = DateTime.Now;
            havePieces = new List<Piece>();
        }

        public static void generateRandomPeerId()
        {
            
            var chars = "abcdef1234567890";
            var stringChars = new char[20];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
                
            }

            var finalString = new String(stringChars);

            Console.WriteLine("id:\n\t" + finalString);

   


            peerID = finalString;
        }
        
        public static string getEventState()
        {
            return getEventState(currentEventState);
        }

        public static string getEventState(EventState eventState)
        {
            switch(eventState)
            {
                case(EventState.started):
                    return Keys.startedState;
                case(EventState.stopped):
                    return Keys.stoppedState;
                case(EventState.completed):
                    return Keys.completedState;
                default:
                    return Keys.nullState;
            }
                    

            }


        public static Piece getPiece(byte[] indexe)
        {
            foreach(Piece piece in actualPieces)
            {
                if (piece.equals(indexe))
                    return piece;
                
            }

            return null;
        }

        }



    }

    class Keys
    {

        public static int FAIL = 0x0;
        public static int SUCCESS = 0x1;

        public static string announceUrlString = "announce";
        public static string announceListString = "announce-list";
        public static string commentString = "comment";
        public static string createdByString = "created by";
        public static string creationDateString = "creation date";
        public static string infoString = "info";
        public static string lengthString = "length";
        public static string checksumString = "md5sum";
        public static string nameString = "name";
        public static string pieceLengthString = "piece length";
        public static string piecesString = "pieces";

        public static string infoHash = "info_hash";
        public static string peerId = "peer_id";
        public static string port = "port";
        public static string uploaded = "uploaded";
        public static string downloaded = "downloaded";
        public static string left = "left";
        public static string ip = "ip";
        public static string numWant = "numwant";
        public static string eventState = "event";
        public static string compact = "compact";
        public static string noPeerId = "no_peer_id";
            

        public static string postString = "POST";

        public static string startedState = "started";
        public static string stoppedState = "stopped";
        public static string completedState = "completed";
        public static string nullState = "nullstate";

        public static string failureReason = "failure reason";
        public static string warningMessage = "warning message";
        public static string interval = "interval";
        public static string minInterval = "min interval";
        public static string trackerId = "tracker id";
        public static string complete = "complete";
        public static string incomplete = "incomplete";
        public static string peers = "peers";
        public static string pstrlen = "pstrlen";
        public static string pstr = "pstr";
        public static string reserved = "reserved";

        public static string protocol = "BitTorrent protocol";
        public static int protocolLength = 19;

        public static int maxRequestLength = (Int32)Math.Pow(2, 14);
        



    }


class Piece
{
    public byte[] pieceIndex;
  public  Boolean have;
  public List<byte> data;
   public int length;
   public byte[] hash;
   public long pieceOffset;

    public Piece(byte[] index)
    {
        have = false;
        length = 1024;
        data = new List<byte>();
        pieceIndex = index;
        hash = new byte[20];
        Vars.blockOffset = pieceOffset = Vars.blockOffset + Vars.pieceLength;
        
    }

    public void addData(byte[] moreData)
    {

        data.AddRange(moreData);

    }

    public Boolean equals(Piece piece)
    {
        byte[] index = piece.pieceIndex;

        return (pieceIndex[0] == index[0] && pieceIndex[1] == index[1] && pieceIndex[2] == index[2] && pieceIndex[3] == index[4]);
    }

    public Boolean equals(byte[] index)
    {
        return (pieceIndex[0] == index[0] && pieceIndex[1] == index[1] && pieceIndex[2] == index[2] && pieceIndex[3] == index[3]);

    }

}


class Peer
{
    private IPAddress ip;
    private long port;
    private string peerId;


    public IPAddress Ip
    {
        get
        {
            return ip;
        }
        set
        {
            ip = value;
        }
    }

    public long Port
    {
        get
        {
            return port;
        }
        set
        {
            port = value;
        }

    }

    public string PeerId
    {
        get
        {
            return peerId;
        }
        set
        {
            peerId = value;
        }
    }


    public Peer(IPAddress ip, long port, string peerId)
    {
        this.ip = ip;
        this.port = port;
        this.peerId = peerId;
    }

    
    public override string ToString()
    {
        return "<ip: " + ip + ", port: " + port + ">";
    }


}




